# test-emocha-client

> Client side for Emocha coding test

## About

This project uses  Parcel as bundler.  [parceljs.com](http://parceljs.com).

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.

2. Install your dependencies

    ```
    cd path/to/test-emocha-client; npm install
    ```

3. Start the app

    ```
    npm start
    ```

  You should be able to open app in http://localhost:1234

## About

As any project this has many thing to improve.
I used bootstrap and reactstrap to handle layout. The app has basically 3 parts. Widgets, Purchase and Orders 
