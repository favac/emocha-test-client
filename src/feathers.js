// import io from 'socket.io-client'
import feathers from '@feathersjs/client'
import rest from '@feathersjs/rest-client'

const SERVER = process.env.API
// const SERVER = 'http://192.168.1.51:3032'

const app = feathers()

const restClient = rest(SERVER)
app.configure(restClient.fetch(window.fetch))

app.configure(feathers.authentication({ storage: localStorage }))

export default app
