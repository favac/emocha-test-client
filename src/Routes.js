import React from 'react'
import { Route } from 'react-router-dom'
import { Container } from 'reactstrap'

import Widgets from './Components/Widgets'
import Purchase from './Components/Purchase'
import Orders from './Components/Orders'
import Dashboard from './Components/Dashboard'

export default () => {
  return (
    <Container style={{ marginTop: 30 }}>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/widgets" component={Widgets} />
      <Route exact path="/purchase" component={Purchase} />
      <Route exact path="/orders" component={Orders} />
    </Container>
  )
}
