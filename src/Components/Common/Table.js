import React, { Component } from 'react'
import { Table as BootstrapTable } from 'reactstrap'
import app from '../../feathers'
import { Input, Label, FormGroup } from 'reactstrap'

export default class Table extends Component {
  state = {
    records: { data: [], total: 0 },
    page: 1,
    limit: 10,
    search: '',
    loading: false
  }

  timer = null

  loadData = async () => {
    let { sortField, sortOrder, page, limit, search } = this.state
    let { service, searchFields = [], fixedQuery = {} } = this.props
    this.setState({ loading: true })
    let query = {
      $sort: {},
      $skip: (page - 1) * limit,
      $limit: limit,
      ...fixedQuery
    }

    query.$sort[sortField] = sortOrder === 'asc' ? 1 : -1

    if (search.trim() !== '') {
      query.$or = []
      searchFields.forEach(field => {
        query.$or.push({ [field]: { $search: search } })
      })
    }

    let records = await app.service(service).find({ query })

    this.setState({ records, loading: false })
  }

  sort = sortField => {
    let { sortOrder } = this.state
    this.setState(
      {
        sortField,
        sortOrder: sortOrder === 'asc' ? 'desc' : 'asc'
      },
      () => {
        this.loadData()
      }
    )
  }

  onNextPage = e => {
    let { page, limit, records } = this.state
    e.preventDefault()
    if (page * limit > records.total) return
    this.setState({ page: page + 1 }, () => {
      this.loadData()
    })
  }

  onPreviousPage = e => {
    let { page } = this.state
    e.preventDefault()
    if (page === 1) return
    this.setState({ page: page - 1 }, () => {
      this.loadData()
    })
  }

  componentDidMount = () => {
    this.setState(
      {
        sortField: this.props.sortField || this.props.columns[0].field,
        sortOrder: this.props.sortOrder || 'asc'
      },
      () => {
        this.loadData()
      }
    )
  }

  onSearch = e => {
    let {
      target: { value }
    } = e
    this.setState({ search: value, page: 1 })
    if (this.timer) clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      this.loadData()
    }, 800)
  }

  render() {
    let { records, sortField, sortOrder, page, limit, loading } = this.state
    let { columns, search } = this.props
    return (
      <div>
        {search && (
          <FormGroup>
            <Label for="exampleSearch">Search</Label>
            <Input
              type="search"
              name="search"
              placeholder="Type to Search"
              onChange={this.onSearch}
            />
          </FormGroup>
        )}

        <div>
          <span style={{ float: 'left' }}>
            Showing records {(page - 1) * limit + 1} to{' '}
            {records.total < page * limit ? records.total : page * limit} of{' '}
            {records.total} {loading && <i className="fas fa-sync fa-spin" />}
          </span>
          <a style={{ float: 'right' }} href="" onClick={this.onNextPage}>
            {`Next >>`}
          </a>
          <span style={{ float: 'right', marginRight: 10 }}>Page {page}</span>
          <a
            style={{ float: 'right', marginRight: 10 }}
            href=""
            onClick={this.onPreviousPage}
          >
            {` << Previous`}
          </a>
        </div>
        <BootstrapTable hover>
          <thead>
            <tr>
              {columns.map(column => {
                let style = column.style || {}
                return (
                  <th
                    key={`th_${column.field}`}
                    style={{ cursor: 'pointer', ...style }}
                    onClick={() => {
                      this.sort(column.field)
                    }}
                  >
                    {column.title}&nbsp;
                    {sortField === column.field && sortOrder === 'asc' && (
                      <i className="fas fa-caret-up" />
                    )}
                    {sortField === column.field && sortOrder === 'desc' && (
                      <i className="fas fa-caret-down" />
                    )}
                  </th>
                )
              })}
            </tr>
          </thead>
          <tbody>
            {records.data.map((record, index) => {
              return (
                <tr key={`record_${index}`}>
                  {columns.map(column => {
                    return (
                      <td
                        key={`td_${column.field}_${index}`}
                        style={column.style}
                      >
                        {record[column.field]}
                      </td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </BootstrapTable>
      </div>
    )
  }
}
