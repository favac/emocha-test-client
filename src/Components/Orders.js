import React, { Component } from 'react'
import Table from './Common/Table'
const columns = [
  {
    field: 'order_code',
    title: 'Order Code'
  },
  {
    field: 'date',
    title: 'Date'
  },
  { field: 'total_items', title: 'Items', style: { textAlign: 'right' } },
  { field: 'total_order', title: 'Total', style: { textAlign: 'right' } }
]

export default class Orders extends Component {
  render() {
    return (
      <div>
        <h1>Browse Orders</h1>
        <br />
        <Table
          service="orders"
          columns={columns}
          searchFields={['order_code']}
          search={true}
        />
      </div>
    )
  }
}
