import React, { Component } from 'react'
import { Jumbotron } from 'reactstrap'

export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <Jumbotron>
          <h1 className="display-3">Hello, world!</h1>
          <p className="lead">This is a code test for Emocha.</p>
          <hr className="my-2" />
          <p>It uses React on client side and Node for backend.</p>
        </Jumbotron>
      </div>
    )
  }
}
