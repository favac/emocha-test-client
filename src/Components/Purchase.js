import React, { Component } from 'react'
import {
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button,
  Alert,
  Table
} from 'reactstrap'
import app from '../feathers'
import moment from 'moment'
import { Redirect } from 'react-router-dom'

// laveragin in feathers client api to handle calls to api
const categoriesService = app.service('categories')
const colorsService = app.service('colors')
const sizesService = app.service('sizes')
const widgetsService = app.service('widgets')
const ordersService = app.service('orders')
const ordersWidgetsService = app.service('orders-widgets')

// auxiliar to set selected items in state
const selectedMap = {
  categories: 'selectedCategory',
  colors: 'selectedColor',
  sizes: 'selectedSize'
}

export default class Purchase extends Component {
  state = {
    categories: { data: [], total: 0 },
    colors: { data: [], total: 0 },
    sizes: { data: [], total: 0 },
    selectedCategory: {},
    selectedColor: {},
    selectedSize: {},
    selectedQuantity: 1,
    orderItems: [],
    totalOrder: 0,
    showError: false,
    error: '',
    goToOrders: false
  }

  loadData = async () => {
    let categories = await categoriesService.find()
    let colors = await colorsService.find()
    let sizes = await sizesService.find({ query: { $sort: { code: 1 } } })
    let ordersQtty = await ordersService.find({
      query: {
        $limit: 0,
        date: {
          $lt: moment()
            .endOf('day')
            .format('YYYY-MM-DD HH:mm:ss'),
          $gt: moment()
            .startOf('day')
            .format('YYYY-MM-DD HH:mm:ss')
        }
      }
    })

    // with the number of orders I will create the order_code
    let sufix = ordersQtty.total
    if (ordersQtty.total < 9) {
      sufix = `0${ordersQtty.total + 1}`
    }
    let order_code = moment().format('YYYYMMDD') + sufix

    let orderCodeSaveResult = await ordersService.create({
      order_code,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      total_items: 0,
      total_order: 0
    })

    // console.log('order_code', order_code)
    this.setState({
      categories,
      colors,
      sizes,
      selectedCategory: categories.data[0],
      selectedColor: colors.data[0],
      selectedSize: sizes.data[0],
      selectedTotal: categories.data[0].price,
      order_code,
      order_id: orderCodeSaveResult._id
    })
  }

  change = e => {
    let {
      target: { value, name }
    } = e
    let selectedItem = this.state[name].data.find(c => {
      return c.code === value
    })
    this.setState(
      {
        [selectedMap[name]]: selectedItem,
        error: '',
        showError: false
      },
      () => {
        if (name === 'categories') {
          this.setItemTotal()
        }
      }
    )
  }

  setItemTotal = () => {
    let { selectedCategory, selectedQuantity } = this.state
    let total = selectedCategory.price * selectedQuantity
    this.setState({ selectedTotal: total })
  }

  onChangeQuantity = e => {
    let { selectedCategory } = this.state
    let {
      target: { value }
    } = e
    let total = selectedCategory.price * value
    this.setState({ selectedQuantity: value }, () => {
      this.setItemTotal()
    })
  }

  getCode = () => {
    let { selectedCategory, selectedColor, selectedSize } = this.state
    return selectedCategory.code + selectedColor.code + selectedSize.code
  }

  onAddToOrder = async () => {
    let { selectedQuantity, orderItems, order_code } = this.state
    let code = this.getCode()
    let widgetResult = await widgetsService.find({ query: { code } })
    let widget = widgetResult.data[0]
    let alreadyInOrder = orderItems.findIndex(oi => oi.code === code)

    // validate number
    if (selectedQuantity === '' || selectedQuantity < 1) {
      this.setState({
        showError: true,
        error: 'Must set a valid number'
      })
      return
    }
    // validate if the product is in the order
    if (alreadyInOrder >= 0) {
      this.setState({
        showError: true,
        error: 'This product is already in this order!'
      })
      return
      // validate if there is enough units of product
    } else if (widget.qtty - widget.sell < selectedQuantity) {
      this.setState({
        showError: true,
        error: 'We are sorry. There are not enough existence of this product :('
      })
      return
    } else {
      let total = widget.price * selectedQuantity
      let totalOrder =
        orderItems.reduce((prev, next) => {
          return prev + next.total_cost
        }, 0) + total
      this.setState({
        showError: false,
        error: '',
        orderItems: [
          {
            order_code,
            code,
            description: widget.description,
            qtty: selectedQuantity,
            total_cost: total
          },
          ...orderItems
        ],
        totalOrder
      })
    }
  }

  onCheckout = async () => {
    let { orderItems, order_code, totalOrder, order_id } = this.state
    let editOrderResult = await ordersService.patch(order_id, {
      total_order: totalOrder,
      total_items: orderItems.length
    })
    let saveItemsResult = await ordersWidgetsService.create(orderItems)
    this.setState({ goToOrders: true })
  }

  componentDidMount = () => {
    this.loadData()
  }

  render() {
    let {
      categories,
      colors,
      sizes,
      selectedCategory,
      selectedColor,
      selectedSize,
      selectedQuantity,
      orderItems,
      showError,
      error,
      totalOrder,
      selectedTotal,
      order_code,
      goToOrders
    } = this.state
    if (goToOrders) {
      return <Redirect to="/orders" />
    }
    return (
      <div>
        <h1>Purchase</h1>
        <br />
        <Row>
          <Col md="6">
            <FormGroup>
              <Label>Select Category</Label>
              <Input
                type="select"
                onChange={this.change}
                name="categories"
                value={selectedCategory.code}
              >
                {categories.data.map(category => {
                  return (
                    <option
                      key={`category_${category.code}`}
                      value={category.code}
                    >
                      {category.name}
                    </option>
                  )
                })}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label>Select Size</Label>
              <Input
                type="select"
                onChange={this.change}
                name="sizes"
                value={selectedSize.code}
              >
                {sizes.data.map(size => {
                  return (
                    <option key={`size_${size.code}`} value={size.code}>
                      {size.name}
                    </option>
                  )
                })}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label>Select Color</Label>
              <div style={{ clear: 'both' }} />
              {colors.data.map(color => {
                return (
                  <div
                    key={`color_${color.code}`}
                    onClick={() => {
                      this.change({
                        target: { value: color.code, name: 'colors' }
                      })
                    }}
                    style={{
                      cursor: 'pointer',
                      width: 45,
                      height: 45,
                      backgroundColor: color.hex_code,
                      marginRight: 15,
                      marginBottom: 15,
                      float: 'left',
                      border:
                        color.code === selectedColor.code
                          ? '3px solid lightgray'
                          : 'none',
                      borderRadius: 5
                    }}
                  >
                    {' '}
                  </div>
                )
              })}
            </FormGroup>
          </Col>
          <Col md="6" style={{ textAlign: 'center' }}>
            <h3>Your Product</h3>
            <h4>Code: {this.getCode()}</h4>
            <i
              className={selectedCategory.icon}
              style={{
                fontSize: selectedSize.web_size,
                color: selectedColor.hex_code
              }}
            />
            <br />
            <span>$ {selectedTotal}</span>
            <br />
            <Row>
              <Col md="6">
                <FormGroup style={{ textAlign: 'left' }}>
                  <Label>Quantity</Label>
                  <Input
                    type="number"
                    value={selectedQuantity}
                    onChange={this.onChangeQuantity}
                  />
                </FormGroup>
              </Col>
              <Col md="6">
                <Button
                  color="success"
                  onClick={this.onAddToOrder}
                  style={{ marginTop: 25 }}
                >
                  Add To Order
                </Button>
              </Col>
            </Row>

            <Row>
              <Col md="12">
                {showError && <Alert color="danger">{error}</Alert>}
              </Col>
            </Row>
          </Col>
        </Row>
        <br />
        <Row>
          <Col md="12">
            <h3>
              This order: ${totalOrder}
              <small>
                <span style={{ float: 'right' }}>Order # {order_code}</span>
              </small>
            </h3>
            <Table>
              <thead>
                <tr>
                  <th>Code</th>
                  <th>Description</th>
                  <th style={{ textAlign: 'right' }}>Quantity</th>
                  <th style={{ textAlign: 'right' }}>Cost</th>
                </tr>
              </thead>
              <tbody>
                {orderItems.map((item, index) => {
                  return (
                    <tr key={`order_item_${index}`}>
                      <td>{item.code}</td>
                      <td>{item.description}</td>
                      <td style={{ textAlign: 'right' }}>{item.qtty}</td>
                      <td style={{ textAlign: 'right' }}>
                        $ {item.total_cost}
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
            {orderItems.length > 0 && (
              <Button
                style={{ float: 'right' }}
                color="success"
                onClick={this.onCheckout}
              >
                Checkout
              </Button>
            )}
          </Col>
        </Row>
      </div>
    )
  }
}
