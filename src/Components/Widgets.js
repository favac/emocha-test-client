import React, { Component } from 'react'
import Table from './Common/Table'

const columns = [
  {
    field: 'description',
    title: 'Description'
  },
  {
    field: 'qtty',
    title: 'Initial Quantity',
    style: { textAlign: 'right' }
  },
  { field: 'sell', title: 'Sold', style: { textAlign: 'right' } },
  {
    field: 'code',
    title: 'Code'
  }
]

export default class Widgets extends Component {
  render() {
    return (
      <div>
        <h1>Browse Widgets</h1>
        <br />
        <Table
          service="widgets"
          columns={columns}
          searchFields={['description', 'code']}
          search={true}
        />
      </div>
    )
  }
}
