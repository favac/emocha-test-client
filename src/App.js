import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Nav from './Components/Nav'
import Routes from './Routes'
export default class App extends Component {
  render() {
    return (
      <Router>
        <Nav />
        <Routes />
      </Router>
    )
  }
}
